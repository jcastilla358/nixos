# My system config

This repo tracks my adventures in the reproducible world of nix.


## Random notes

### Docker subsystem
NixOS is sometimes not very convenient.
VScode is kinda broken and many things just don't work out of the box. Therefore I decided to create my own NSU (**N**ixOS **S**ubsystem for **U**buntu). I share xserver socket so i can run gui apps in it.

Relevant links:
https://stackoverflow.com/a/25280523
