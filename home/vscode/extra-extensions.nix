[
  {
    name = "nix-lsp";
    publisher = "aaronduino";
    version = "0.0.1";
    sha256 = "190pqcxlz98grigbppkrj5zvwk8d9si70na7jmilypaxn3zdmm9w";
  }
  {
    name = "nixfmt-vscode";
    publisher = "brettm12345";
    version = "0.0.1";
    sha256 = "07w35c69vk1l6vipnq3qfack36qcszqxn8j3v332bl0w6m02aa7k";
  }
  {
    name = "nixpkgs-fmt";
    publisher = "b4dm4n";
    version = "0.0.1";
    sha256 = "1gvjqy54myss4w1x55lnyj2l887xcnxc141df85ikmw1gr9s8gdz";
  }
  {
    name = "rust";
    publisher = "rust-lang";
    version = "0.7.8";
    sha256 = "039ns854v1k4jb9xqknrjkj8lf62nfcpfn0716ancmjc4f0xlzb3";
  }
  {
    name = "project-manager";
    publisher = "alefragnani";
    version = "12.0.1";
    sha256 = "1bckjq1dw2mwr1zxx3dxs4b2arvnxcr32af2gxlkh4s26hvp9n1v";
  }
  {
    name = "i18n-ally";
    publisher = "antfu";
    version = "3.4.20";
    sha256 = "0bnm8pyk8z1biff0wccad430jh2cyd2r3sgihhj25cwkfjszj348";
  }
  {
    name = "svelte-intellisense";
    publisher = "ardenivanov";
    version = "0.7.1";
    sha256 = "1g28sq3wgpsyq7kbd742pg02r0hyp8vwa2rgvx0kyy021hr8c27w";
  }
  {
    name = "nix-env-selector";
    publisher = "arrterian";
    version = "1.0.6";
    sha256 = "19k60nrhimwf61ybnn1qqb0n0zh2wdr8pp1x5bla9r76hz5srqdl";
  }
  {
    name = "markdown-mermaid";
    publisher = "bierner";
    version = "1.9.2";
    sha256 = "0sq11jx8zggf9gf8ch1nhm6fxrnbr64bm58cz73y554wmpvwgfyc";
  }
  {
    name = "path-intellisense";
    publisher = "christian-kohler";
    version = "2.3.0";
    sha256 = "1wyp3k4gci1i64nrry12il6yflhki18gq2498z3nlsx4yi36jb3l";
  }
  {
    name = "vscode-css-modules";
    publisher = "clinyong";
    version = "0.2.3";
    sha256 = "1shyds331lzk3f8nprcccbkrbr4qkc43p190d6yvf2rb92x9l1mx";
  }
  {
    name = "markdown-table-prettify";
    publisher = "darkriszty";
    version = "3.2.0";
    sha256 = "1jmv35v5vn9j0i9rwap2pq5ksrv75l2pgsh49xbdb79s1r5g0jxj";
  }
  {
    name = "dart-code";
    publisher = "Dart-Code";
    version = "3.20.1";
    sha256 = "01hii25w3wv81kvxm2i54y5jzifics1m5r6pwszv3cryb1vzf6ib";
  }
  {
    name = "flutter";
    publisher = "Dart-Code";
    version = "3.20.0";
    sha256 = "00pvzakjrxcaaq345cs6jxz351sw48jc6vrws93a57ykka2dy2bs";
  }
  {
    name = "vscode-eslint";
    publisher = "dbaeumer";
    version = "2.1.17";
    sha256 = "1c895irqj99kkhikd2f7hfx6i7ml6vhdy6rlbsbhmsfiw0vhqzkb";
  }
  {
    name = "i3";
    publisher = "dcasella";
    version = "0.0.1";
    sha256 = "0lv6slag3lc3idkkq9lllbyvm55p1b52dmrrysr922sf06gm0z9j";
  }
  {
    name = "xml";
    publisher = "DotJoshJohnson";
    version = "2.5.1";
    sha256 = "1v4x6yhzny1f8f4jzm4g7vqmqg5bqchyx4n25mkgvw2xp6yls037";
  }
  {
    name = "next-js";
    publisher = "foxundermoon";
    version = "0.0.2";
    sha256 = "0wlaghysnlg19qxvpk2wkihngk55dgnw7qbrvixb30i9rq1acqia";
  }
  {
    name = "vscode-yarn";
    publisher = "gamunu";
    version = "1.7.1";
    sha256 = "068n47i7df37q3rmpnq522ichh5zba0f59hzj22qcfz6gwwax1cg";
  }
  {
    name = "mdmath";
    publisher = "goessner";
    version = "2.5.1";
    sha256 = "0cdwic1rc139zi0f4ahh7wx0b0shm2bbzadk4yw3bksxv9dqcjih";
  }
  {
    name = "latte";
    publisher = "Kasik96";
    version = "0.17.0";
    sha256 = "1iyrf2b4qqfabfsa32j8zp17ygvgvg2r4vnm5allqgwpnipas8b0";
  }
  {
    name = "graphql-for-vscode";
    publisher = "kumar-harsh";
    version = "1.15.3";
    sha256 = "1x4vwl4sdgxq8frh8fbyxj5ck14cjwslhb0k2kfp6hdfvbmpw2fh";
  }
  {
    name = "sftp";
    publisher = "liximomo";
    version = "1.12.9";
    sha256 = "1r8y5y59ibcdqy992xvpys32imsyv290z3xrzkm3f3xfw4a5d3v6";
  }
  {
    name = "import-sorter";
    publisher = "mike-co";
    version = "3.3.1";
    sha256 = "0z6biw0a06as63nb0pbxpk5j3dxwk7n1v14f4dk7f9hh7j69qp2s";
  }
  {
    name = "language-gettext";
    publisher = "mrorz";
    version = "0.2.0";
    sha256 = "0q17d4k6bj8d4sbr5ip54vm06ydgcv8ajgd7hhi3vmjwfsdk8ryy";
  }
  {
    name = "jupyter";
    publisher = "ms-toolsai";
    version = "2021.4.641214696";
    sha256 = "0ni4664hp4031ff81w2b5pvn9h9j3l4s3gpkhjnsxn96xgqgzmbc";
  }
  {
    name = "remote-containers";
    publisher = "ms-vscode-remote";
    version = "0.163.2";
    sha256 = "0dvs5f1a4mnbg509hphp5wchn1n79r0rsf7qjv0wgs9phlrlaijs";
  }
  {
    name = "remote-ssh-edit";
    publisher = "ms-vscode-remote";
    version = "0.65.1";
    sha256 = "0ajbn3iyhxajj7v6j5sh74j9bm6qh2sx0964vlillbiwr5i845f0";
  }
  {
    name = "remote-wsl";
    publisher = "ms-vscode-remote";
    version = "0.54.3";
    sha256 = "0pb8qx4j8i1j94kgk75ikjh5z30iiwjrimq0zmk12lmi5vd484yb";
  }
  {
    name = "vscode-remote-extensionpack";
    publisher = "ms-vscode-remote";
    version = "0.20.0";
    sha256 = "04wrbfsb8p258pnmqifhc9immsbv9xb6j3fxw9hzvw6iqx2v3dbi";
  }
  {
    name = "vscode-react-native";
    publisher = "msjsdiag";
    version = "1.4.1";
    sha256 = "08m0zb17q3nrh7si9pyzy8bwyn3k4wpg90yxgavhpmzhnah15800";
  }
  {
    name = "postscript";
    publisher = "mxschmitt";
    version = "1.0.1";
    sha256 = "01s4i8s2f73zcf3mj9k7kisf06k9hnd0m85lkfqc06962rlbinxg";
  }
  {
    name = "vetur";
    publisher = "octref";
    version = "0.33.1";
    sha256 = "1iq2h87j7dr4vf9zgzihd5q4d95grc0iirz68az5dnvy19nvfv57";
  }
  {
    name = "vscode-jest";
    publisher = "Orta";
    version = "3.2.0";
    sha256 = "1qhhy3q5lmdmgw25vmyx69h37i2vbpjxca46jra86vm6kdwglc36";
  }
  {
    name = "java";
    publisher = "redhat";
    version = "0.76.0";
    sha256 = "0xb9brki4s00piv4kqgz6idm16nk6x1j6502jljz7y9pif38z32y";
  }
  {
    name = "vscode-table-formatter";
    publisher = "shuworks";
    version = "1.2.1";
    sha256 = "0kr99887yickzi1x3yx13qh43zj1lacl2jcqa7jk2sffj7w2mcaf";
  }
  {
    name = "mdx";
    publisher = "silvenon";
    version = "0.1.0";
    sha256 = "1mzsqgv0zdlj886kh1yx1zr966yc8hqwmiqrb1532xbmgyy6adz3";
  }
  {
    name = "autoimport";
    publisher = "steoates";
    version = "1.5.3";
    sha256 = "1xm11f1kph75kbxq2hmgcb4fp2nafmbyb58vix39p7988vdl51fi";
  }
  {
    name = "code-spell-checker-czech";
    publisher = "streetsidesoftware";
    version = "0.1.6";
    sha256 = "0cqigc40q04jf741439zg3pfrk6ffq3dyi51izyx84nq042q817i";
  }
  {
    name = "svelte-vscode";
    publisher = "svelte";
    version = "104.6.4";
    sha256 = "0p75racdw1zxcx2v8ch1kkcflqxixmjhqap5xwb0d8yk892ldmz2";
  }
  {
    name = "pdf";
    publisher = "tomoki1207";
    version = "1.1.0";
    sha256 = "0pcs4iy77v4f04f8m9w2rpdzfq7sqbspr7f2sm1fv7bm515qgsvb";
  }
  {
    name = "errorlens";
    publisher = "usernamehw";
    version = "3.2.5";
    sha256 = "0zdmk4k7x4qyb3zbnxiq75kzky9187nl5y23wwzjbi2mc4kbg3bk";
  }
  {
    name = "vscodeintellicode";
    publisher = "VisualStudioExptTeam";
    version = "1.2.11";
    sha256 = "057szin28d4sz18b1232xjhf5jjnw2574q34vs3npblhc1jb5y3p";
  }
  {
    name = "vscode-arduino";
    publisher = "vsciot-vscode";
    version = "0.3.5";
    sha256 = "0gfcyac0jc0lrq3iq2p009a8awaxdl91vcdnnkggqw4f8mjvkgdz";
  }
  {
    name = "vscode-java-debug";
    publisher = "vscjava";
    version = "0.32.0";
    sha256 = "0n49bh3cq2h9v4pzcw9rihffvywqc37ikljhsjdg161r46q2f2qf";
  }
  {
    name = "vscode-java-dependency";
    publisher = "vscjava";
    version = "0.18.1";
    sha256 = "0nwgrvjziz4m136j9n0vxrrpynrxjxygl011ihxnj2600pq7qbqg";
  }
  {
    name = "vscode-java-pack";
    publisher = "vscjava";
    version = "0.13.0";
    sha256 = "1mn8wqhsxkrvzzlan5nqkryb3rsz5bxnq0b180960i493273fkw4";
  }
  {
    name = "vscode-java-test";
    publisher = "vscjava";
    version = "0.28.1";
    sha256 = "1ry2rhk79zhyslqccbf50y067plgpmf6n0yalzmd9sbm4a0vbs3q";
  }
  {
    name = "vscode-maven";
    publisher = "vscjava";
    version = "0.28.0";
    sha256 = "11s16jg2yr888hikb6p8q2lm8jra0gbzg3msmzdzlbk3ipy6wnim";
  }
  {
    name = "vscode-icons";
    publisher = "vscode-icons-team";
    version = "11.1.0";
    sha256 = "1xrz9f0nckx29wxpmlj1dqqiaal3002xwgzz5p9iss119sxgpwrx";
  }
  {
    name = "vscode-import-cost";
    publisher = "wix";
    version = "2.15.0";
    sha256 = "0d3b6654cdck1syn74vmmd1jmgkrw5v4c4cyrhdxbhggkip732bc";
  }
]
